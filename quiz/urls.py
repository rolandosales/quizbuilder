from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'signup/$', views.signup, name='signup'),
    url(r'^quiz/(?P<pk>\d+)/$', views.quiz_detail, name='quiz_detail'),
    url(r'^quiz/(?P<pk>\d+)/answer$', views.quiz_answer, name='quiz_answer'),
    url(r'^quiz/(?P<pk>\d+)/submit$', views.quiz_submit, name='quiz_submit'),
    url(r'^session/(?P<pk>\d+)/$',
        views.quiz_session, name='quiz_session'),
]
