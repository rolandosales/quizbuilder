# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Quiz, Question, Choice

admin.site.register(Question)
admin.site.register(Quiz)
admin.site.register(Choice)
