# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone


class Quiz(models.Model):
    title = models.CharField(max_length=200)
    creation_date = models.DateTimeField(default=timezone.now)
    expiration_date = models.DateTimeField(blank=True, null=True)

    def is_expired(self):
        if self.expiration_date is None:
            return False
        return self.expiration_date <= timezone.now()

    def get_total(self):
        questions = Question.objects.filter(quiz=self)
        total = 0
        for question in questions:
            total += 1
        return total

    def __str__(self):
        return self.title


class Question(models.Model):
    question = models.CharField(max_length=200)
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE)

    def get_correct_ans(self):
        correct_ans = Choice.objects.get(question=self, is_correct=True)
        return correct_ans

    def __str__(self):
        return str(self.quiz) + " " + self.question


class Choice(models.Model):
    label = models.CharField(max_length=200)
    is_correct = models.BooleanField(default=False)
    question = models.ForeignKey(Question, related_name='choices',
                                 on_delete=models.CASCADE)

    def set_correct(self):
        self.is_correct = True
        self.save()

    def __str__(self):
        return str(self.question) + " " + self.label


class Session(models.Model):
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE)
    session_date = models.DateTimeField(default=timezone.now)

    def get_score(self):
        answers = Answer.objects.filter(session=self)
        count = 0
        for answer in answers:
            if answer.is_correct():
                count += 1
        return count

    def __str__(self):
        return str(self.session_date)


class Answer(models.Model):
    session = models.ForeignKey(Session, related_name='answers',
                                on_delete=models.CASCADE)
    choice = models.ForeignKey(Choice, null=True, on_delete=models.CASCADE)

    def is_correct(self):
        correct_ans = self.choice.question.get_correct_ans()
        return self.choice.pk is correct_ans.pk
