# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth import login, authenticate
from .models import Quiz, Session, Question, Choice, Answer


def home(request):
    if request.user.is_authenticated():
        quizzes = Quiz.objects.all()
        sessions = Session.objects.filter(user=request.user)
        sessions = sessions.order_by('-session_date')[:10]
        return render(request, 'quiz/home.html',
                      {'sessions': sessions, 'quizzes': quizzes})
    else:
        quizzes = Quiz.objects.all().order_by('creation_date')
        if request.method == 'POST':
            form = AuthenticationForm(None, request.POST)
            if form.is_valid():
                username = form.cleaned_data.get('username')
                raw_password = form.cleaned_data.get('password')
                user = authenticate(username=username, password=raw_password)
                login(request, user)
                return redirect('home')
            else:
                return render(request, 'registration/login.html',
                              {'form': form, 'quizzes': quizzes})
        else:
            form = AuthenticationForm()
        return render(request, 'registration/login.html',
                      {'form': form, 'quizzes': quizzes})


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')
    else:
        form = UserCreationForm()
    return render(request, 'registration/signup.html', {'form': form})


def quiz_detail(request, pk):
    quiz = get_object_or_404(Quiz, pk=pk)
    sessions = Session.objects.filter(user=request.user, quiz=pk)
    return render(request, 'quiz/quiz_detail.html',
                  {'quiz': quiz, 'sessions': sessions})


def quiz_session(request, pk):
    session = get_object_or_404(Session, pk=pk)
    return render(request, 'quiz/quiz_session.html',
                  {'session': session})


def quiz_answer(request, pk):
    quiz = get_object_or_404(Quiz, pk=pk)
    questions = Question.objects.filter(quiz=quiz)
    return render(request, 'quiz/quiz_answer.html',
                  {'quiz': quiz, 'questions': questions})


def quiz_submit(request, pk):
    quiz = get_object_or_404(Quiz, pk=pk)
    if request.method == 'POST':
        questions = Question.objects.filter(quiz=quiz)
        new_session = Session.objects.create(user=request.user,
                                             quiz=quiz)
        new_session.save()

        for question in questions:
            choice = request.POST.get(str(question.pk), None)
            choice_obj = Choice.objects.get(pk=choice)
            answer = Answer.objects.create(session=new_session,
                                           choice=choice_obj)
            answer.save()

    return render(request, 'quiz/quiz_submit.html',
                  {'session': new_session})
